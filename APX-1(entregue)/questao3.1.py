
placar_jogo = []


def trocar_competidor(competidor):   # troca do competidor entre as rodadas

    if competidor == competidor_2:
        return competidor_1
    else:
        return competidor_2

def rodada(palitos_atuais, competidor):  # rodada atual

    if palitos_atuais ==0:
        placar_jogo.append(trocar_competidor(competidor))           # acabou, trocar competidor

    else:
        rodada(palitos_atuais -1, trocar_competidor(competidor))   # retira 1 palito

        if palitos_atuais >1:
            rodada(palitos_atuais -2 , trocar_competidor(competidor)) # retira 2 palito

        if palitos_atuais > 3:
            rodada(palitos_atuais - 4, trocar_competidor(competidor)) # retira 4 palito


vence_competidor_1 = 0
vence_competidor_2 = 0
    
palitos_iniciais = int(input("Entre com o numero de palitos para começar o jogo:  "))  # entrada de dados
competidor_1= input("Qual o nome do primeiro competidor? ")
competidor_2= input("Qual o nome do segundo competidor? ")



rodada(palitos_iniciais, competidor_1)

for i in placar_jogo:
    if i == competidor_1:
        vence_competidor_1 += 1
    else:
        vence_competidor_2 += 1

print(competidor_1, vence_competidor_1)

print(competidor_2, vence_competidor_2)