


global matriz

matriz=[]

maior=0
lista_vizinhos_primos=[]


def formata_matriz():

    print("")

    sequencia_numeros = input("Entre com uma sequência de números: ")
    while len(sequencia_numeros) > 0:

        sequencia_numeros_int = sequencia_numeros.split()

        for i in range(len(sequencia_numeros_int)):
            sequencia_numeros_int[i] = int(sequencia_numeros_int[i])
        matriz.append(sequencia_numeros_int)
        sequencia_numeros = input("Entre com uma sequência de números: ")

    return(matriz)





def alinha_matriz():
    
    maior_len = 1
    for i in range(len(matriz)):
        
        for j in range(len(matriz[i])):

            matriz[i][j]=str(matriz[i][j]) 

                       

            if maior_len<=len(matriz[i][j]):  
                maior_len=len(matriz[i][j])          
    

    for i in range(len(matriz)):
        
        for j in range(len(matriz[i])):

            if len(matriz[i][j]) < maior_len:

                matriz[i][j]=matriz[i][j].rjust((maior_len - len(matriz[i][j])+1)," ") 
  
def imprime_matriz():   # imprime a matriz no formato correto

    print("")
    print("Conteúdo da Matriz: ")
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):

            matriz[i][j]=str(matriz[i][j])

            print( matriz[i][j], end=' ') 
        print(" ")
    print(" ")
    return(matriz)


def primos(matriz):                     # verifica se são Primos
   
    i = 0
    
    k = 1

    divisoes = 0
    
    lista_primos=[]

    print(matriz)

    for i in range(len(matriz)):             # rodar pelas linhas da matriz
        
        for j in range(len(matriz[i])):        # rodar pelas colunas da matriz

            matriz[i][j]=int(matriz[i][j])

            for k in range(matriz[i][j]):
                k = k + 1

                if matriz[i][j] % k == 0:
                    divisoes = divisoes + 1
                    k = k + 1
                    
                else:
                    k = k + 1                    

            if divisoes == 2:
                
                lista_primos.append(matriz[i][j])

            divisoes = 0

    print("primos da matriz: ",lista_primos)


def numero_primo(m):
   
    i = 0
    k = 1

    divisoes = 0

    m=int(m)

    for k in range(m):
        k = k + 1

        if m % k == 0:
            divisoes = divisoes + 1
            k = k + 1
            
        else:
            k = k + 1                    

    if divisoes == 2:
        
        primo=True

    else:
        primo=False

    return(primo)


def vizinhos_primos(matriz, i, j):  #verifica os possíveis 8 vizinhos do elemento da matriz, verifica se existem e depois verifica se é primo

    cont_primos = 0
    
    if j-1 >= 0 and numero_primo(matriz[i][j-1]) == True:
        cont_primos +=1
    
    if j+1 < len(matriz[i]) and numero_primo(matriz[i][j+1]) == True:
        cont_primos +=1
    
    if i-1 >= 0:
        if numero_primo(matriz[i-1][j]) == True:
            cont_primos +=1
        if j-1 >= 0 and numero_primo(matriz[i-1][j-1]) == True:
            cont_primos +=1
        if j+1 < len(matriz[i]) and numero_primo(matriz[i-1][j+1]) == True:
            cont_primos +=1

    
    if i+1 < len(matriz):
        if numero_primo(matriz[i+1][j]) == True:
            cont_primos +=1
        if j-1 >= 0 and numero_primo(matriz[i+1][j-1]) == True:
            cont_primos +=1
        if j+1 < len(matriz[i]) and numero_primo(matriz[i+1][j+1]) == True:
            cont_primos +=1
   
    return cont_primos


def meus_vizinhos_primos(): 

    primos_vizinhos=0 

    print("Posição das Células com Quantidade Par de Números Primos: ")
    for i in range(len(matriz)):
        
        for j in range(len(matriz[i])):


            primos_vizinhos=vizinhos_primos(matriz, i, j)

            if primos_vizinhos != 0 and primos_vizinhos % 2 == 0:

                print("(",i+1,",",j+1,")", sep="")
                

    print("Fim da Listagem.")


formata_matriz()
alinha_matriz()
imprime_matriz()
meus_vizinhos_primos()








