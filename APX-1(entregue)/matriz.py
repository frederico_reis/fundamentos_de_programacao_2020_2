def main():
    m = int(input("Digite o número de linhas da matriz A: "))
    n = int(input("Digite o número de colunas da matriz A: "))
    A = le_matriz(m,n)

    print("Matriz lida:")
    imprime_matriz(A)




    for i in range(m):
        for j in range(n):
            if A[i][j] == 0:
                A[i][j] = conta_minas(A,i,j)
                
    print("Matriz de resultado:")
    imprime_matriz(A)   

    

def conta_minas(matriz, i, j):
    """
    (list, int, int) --> int
    Função que recebe como parâmetro uma matriz e uma posição (i,j)
    dessa matriz e devolve a quantidade de posições ao redor da posição
    (i,j) que contêm o valor -1 nessa matriz.

    """
    cont = 0
    
    # Verifica a posição à esquerda
    if j-1 >= 0 and matriz[i][j-1] == -1:
        cont +=1

    # Verifica a posição à direita
    if j+1 < len(matriz[i]) and matriz[i][j+1] == -1:
        cont +=1

    # Verifica as posições acima
    if i-1 >= 0:
        if matriz[i-1][j] == -1:
            cont +=1
        if j-1 >= 0 and matriz[i-1][j-1] == -1:
            cont +=1
        if j+1 < len(matriz[i]) and matriz[i-1][j+1] == -1:
            cont +=1

    # Verifica as posições abaixo
    if i+1 < len(matriz):
        if matriz[i+1][j] == -1:
            cont +=1
        if j-1 >= 0 and matriz[i+1][j-1] == -1:
            cont +=1
        if j+1 < len(matriz[i]) and matriz[i+1][j+1] == -1:
            cont +=1
    
    return cont




























def le_matriz(m, n):
    """
    (int, int) --> list
    
    Função que recebe como entrada um número de linhas m
    e um número de colunas n e então lê uma matriz de inteiros
    tamanho m x n, ou seja, uma lista contendo m listas
    com n elementos cada. A matriz lida é devolvida como
    valor de retorno da função. 
    """

    print("Digite os elementos da matriz de tamanho %dx%d:" %(m,n))
    matriz = []
    for i in range(m):
        matriz.append([])
        for j in range(n):
            elemento = int(input("Elemento da %da. linha, %da. coluna: " %(i+1,j+1)))
            matriz[i].append(elemento)
            
    return matriz

def imprime_matriz(matriz):
    """
    (list) --> None

    Função que recebe uma matriz como entrada, ou seja uma
    lista de listas, e então a imprime.
    A função não tem nenhum valor de retorno.
    """
    
    for linha in matriz:
        for elemento in linha:
            print("%2d" %elemento, end = " ")
        print()

 
################
main()